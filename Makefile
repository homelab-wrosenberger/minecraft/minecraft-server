
.PHONY: keys

install:
	git submodule init && \
	git submodule update
	python3 -m venv env && \
	. env/bin/activate && \
		pip install -r requirements.txt

keys:
	ssh-keygen -t ecdsa -b 521 -f keys/id

activate-key:
	ssh-add keys/id

up:
	. env/bin/activate && \
		ansible-playbook -i ansible/inventories/prod ansible/site.yaml